<?php

namespace core;

interface Iface {

    /**
     * Interface message
     * @param Int $diaDaSemana
     */
    public function message($date);
}
