<?php

namespace core;

class Mensagem implements Iface {

    private $dias;
    private $msgs;

    public function __construct() {
        $this->loadMsgs();
        $this->loadDias();
    }

    /**
     * Retorna uma mensagem para cada dia da semana
     * @param Date $date YYYY-MM-DD
     * @return String
     */
    public function message($date) {

        $dia = date('d', strtotime($date));
        $diaDaSemana = date('w', strtotime($date));
        $mes = date('m', strtotime($date));
        $mensage = date('d/m/Y', strtotime($date));
        $mensage .= '  ' . $this->msgs[$diaDaSemana];
        foreach ($this->dias as $key => $value) {
            if ($key === "{$dia}/{$mes}") {
                $mensage .= " e {$value}";
            }
        }

        return $mensage;
    }

    private function loadMsgs() {
        $this->msgs = [
            0 => 'Mais um domingo!',
            1 => 'Bom dia, e uma ótima segunda-feira para você',
            2 => 'Hoje é terça feira, acorda que você já perdeu 1 dia',
            3 => 'Boa Quarta-feira... meio da semana...',
            4 => 'Boa quinta-feita, estamos quase lá',
            5 => 'Sextou!!! tomar uma?',
            6 => 'Bom sábado, dia da preguiça',
        ];
    }

    private function loadDias() {
        $this->dias = [
            '01/01' => 'Confraternização Universal',
            '21/04' => 'Dia de Tiradentes',
            '01/05' => 'Dia do Trabalho',
            '07/09' => 'Dia da independência do Brasil',
            '12/10' => 'Feriado Nacional, Nossa Senhora de Aparecida',
            '02/11' => 'Dia de Finados',
            '25/12' => 'Feliz Natal',
        ];
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

