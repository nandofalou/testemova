<?php

use core\Mensagem;

require_once('vendor/autoload.php');

$msg = new Mensagem();

$ldias = [
    '2021-12-25',
    '2021-10-27',
    '2021-09-07',
    '2021-10-25',
    '2021-10-24',
    '2021-03-12',
    '2021-05-01',
    '2021-11-02',
];

foreach ($ldias as $value) {
    $message = $msg->message($value);
    echo "<div>{$message}</div>";
}